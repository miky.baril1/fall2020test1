package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTest {

	@Test
	public void getSpeed_test() {
	Car speed1 = new Car (20);
	assertEquals(20,speed1.getSpeed());
	
}
	@Test
	public void getLocation_test() {
		Car location1 = new Car (50);
		assertEquals(50,location1.getLocation());
		
	}
	
	@Test
	public void Car_test() {
	try {
		Car speed = new Car(-1);
		
		fail("The car speed cannot be negative");
	}
	catch (IllegalArgumentException e) {
		
	}
	
}
	
	
	@Test
	public void moveRight() {
		Car speed1 = new Car (20);
		int result = (speed1.getLocation()+speed1.getSpeed());
		assertEquals(70,result);
		
	}
	
	@Test
	public void moveLeft() {
		Car speed1 = new Car (20);
		int result = (speed1.getLocation()-speed1.getSpeed());
		assertEquals(30,result);		
		
	}
	
	@Test
	public void accelerate() {
		Car speed1 = new Car (20);
		int acc1 = speed1.getSpeed()+1;		
		assertEquals(21,acc1);
	}
	
	@Test
	public void stop() {
		Car speed1 = new Car (20);
		int stop1 = speed1.getSpeed()-speed1.getSpeed();
		
		assertEquals(0,stop1);
		
	}
}

