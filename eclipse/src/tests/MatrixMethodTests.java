package tests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	public void matrix_test() {
		int [][] arraydup = {{1,2,3},{4,5,6}};
		int [][]expectedArray = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		utilities.MatrixMehod obj = new utilities.MatrixMehod();
		int[][] result = obj.duplicate(arraydup);
		assertEquals(expectedArray,result);
	}
}
